﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace redraw_Form
{
    public partial class Form1 : Form
    {
        #region properties
        private Image targetImage;
        private List<Color> pixels;
        private List<Color> uniquePixels;

        private bool isImageExample = true;
        public SynchronizationContext mysynchronizationContext;
        private Task CalculGenerationTask;
        private bool play = false;
        public static int iteration = 0;
        private List<graphicBlocAlgo> blocs;
        private TextAlgo textAlgo;
        private string ImageResultPath = Path.GetTempPath() + "result.jpg";
        private string ImageDiffPath = Path.GetTempPath() + "Diff.jpg";
        public double ErrorRate = 0.9990f;

        private myNeuralNetwork myNeuralNetwork;
        #endregion

        #region form interaction

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CalculGenerationTask = new Task(GenerationCalcul);
            mysynchronizationContext = SynchronizationContext.Current;
            AdaptInterfaceToExampleMode();
            InitImageGame(Properties.Resources.MonImage, targetText.Text);
            updateGraphicTimer.Interval = 500;
            updateGraphicTimer.Start();

        }

        private void BestResult_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = graphicBlocAlgo.GenerateBitmapBestResult(blocs);// BitmapFromDNA(BestGene);
            bitmap.Save(ImageResultPath);
            Process.Start(ImageResultPath);
        }

        private async void Pause_Click(object sender, EventArgs e)
        {

            play = !play;
            if (!CalculGenerationTask.Status.Equals(TaskStatus.Running))
            {
                CalculGenerationTask = new Task(GenerationCalcul);
                CalculGenerationTask.Start();
            }
            await CalculGenerationTask;
        }

        private void mutationRate_ValueChanged(object sender, EventArgs e)
        {
            foreach (graphicBlocAlgo bloc in blocs)
                bloc.ga.MutationRate = (float)mutationRate.Value / 100;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            play = false;
            return;
        }

        private void Export_Click(object sender, EventArgs e)
        {
            ExportResult();
        }
        private void ImageRadio_CheckedChanged(object sender, EventArgs e)
        {
            isImageExample = !isImageExample;
            AdaptInterfaceToExampleMode();
        }


        private void Target_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
                Bitmap tempo = new Bitmap(openFileDialog.FileName);

                InitImageGame(tempo, targetText.Text);
                // image file path  
            }
        }
        private void targetText_TextChanged(object sender, EventArgs e)
        {
            InitImageGame(targetImage, targetText.Text);
        }
        private void Difference_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = graphicBlocAlgo.GenerateBitmapDiffResult(blocs, (Bitmap)targetImage);// BitmapFromDNA(BestGene);
            bitmap.Save(ImageDiffPath);
            Process.Start(ImageDiffPath);
        }

        private void updateGraphicTimer_Tick(object sender, EventArgs e)
        {
            PrintDifference();
            PrintBestResult();
            AllResultText.Items.Clear();

            int numberOfItemsOtShow = 10;
            if (textAlgo.ga.Population.Count() < 10)
                numberOfItemsOtShow = 1;
            AllResultText.Items.AddRange(textAlgo.ga.Population.Take(numberOfItemsOtShow).Select(x => new string(x.Genes)).ToArray());
        }

        private void draw_Click(object sender, EventArgs e)
        {
            drawSin(tabPage2, out List<double> input, out List<double> output);
            //myNeuralNetwork = new myNeuralNetwork(3, 1, myNeuralNetwork.Deepness.ReallyDeep);

            //myNeuralNetwork.Train(
            //    new double[][] {
            //        new double[] { 150, 2, 0 },
            //        new double[] { 1002, 56, 1 },
            //        new double[] { 1060, 59, 1 },
            //        new double[] { 200, 3, 0 },
            //        new double[] { 300, 3, 1 },
            //        new double[] { 120, 1, 0 },
            //        new double[] { 80, 1, 0 },
            //    }, 
            //    new double[][] {
            //        new double[] { 0 },
            //        new double[] { 1 },
            //        new double[] { 1 },
            //        new double[] { 0 },
            //        new double[] { 1 },
            //        new double[] { 0 },
            //        new double[] { 0 },
            //    }, 1000);


            //var outputs = myNeuralNetwork.Use(new double[] { 1054, 54, 1 });



            myNeuralNetwork = new myNeuralNetwork(1, 1, myNeuralNetwork.Deepness.ReallyDeep);
            myNeuralNetwork.Train(new double[][] { input.ToArray() }, new double[][] { output.ToArray() }, 100000);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            drawSin2(tabPage2, myNeuralNetwork);
        }
        public void drawSin2(Control control, myNeuralNetwork myNeuralNetwork)
        {
            Graphics g = control.CreateGraphics();
            Pen pen = new Pen(Brushes.Red, 2.0F);
            float x1 = 0;
            float y1 = 0;

            float y2 = 0;

            float yEx = 300;
            float eF = 50;


            for (float x = 0; x < control.Width; x += 0.005F)
            {
                y2 = (float)myNeuralNetwork.Use(new double[] { x / 5 }).First();
                g.DrawLine(pen, x1 * eF, y1 * eF + yEx, x * eF, y2 * eF + yEx);

                x1 = x;
                y1 = y2;
            }
        }
        public void drawSin(Control control, out List<double> input, out List<double> output)
        {
            //if (Completed) return;
            //Completed = true;
            Graphics g = control.CreateGraphics();
            Pen pen = new Pen(Brushes.Black, 2.0F);
            input = new List<double>();
            output = new List<double>();
            float x1 = 0;
            float y1 = 0;

            float y2 = 0;

            float yEx = 300;
            float eF = 50;

            int j = 0;
            for (float x = 0; x < control.Width; x += 0.005F)
            {
                j++;
                y2 = (float)Math.Sin(x / 5) * 2;
                if (j == 1000)
                {
                    j = 0;
                    input.Add(x / 2);
                    output.Add(y2);
                }

                g.DrawLine(pen, x1 * eF, y1 * eF + yEx, x * eF, y2 * eF + yEx);
                x1 = x;
                y1 = y2;




            }
        }
        #endregion

        #region algorithm

        private void GenerationCalcul()
        {
            iteration = 0;

            mysynchronizationContext.Send(o =>
            {
                ImageRadio.Enabled = false;
                TextRadio.Enabled = false;
                Play.Text = "Pause";
            }, null);

            while (play && (isImageExample && (graphicBlocAlgo.GlobalBestFitness(blocs) < ErrorRate) || !isImageExample && (textAlgo.ga.BestFitness < ErrorRate)))
            {
                if (isImageExample)
                {
                    ParallelOptions parallelOptions = new ParallelOptions();
                    parallelOptions.MaxDegreeOfParallelism = 2;
                    Parallel.ForEach(blocs, parallelOptions, bloc =>
                    {
                        iteration++;
                        DateTime BeforeGeneration = DateTime.Now;
                        bloc.GenerationCalcul();
                        DateTime AfterGeneration = DateTime.Now;
                        mysynchronizationContext.Send(o =>
                        {
                            generationTime.Text = (AfterGeneration - BeforeGeneration).Milliseconds.ToString() + "ms";
                            CurrentIteration.Text = iteration.ToString();
                            BestFitness.Text = graphicBlocAlgo.GlobalBestFitness(blocs).ToString();// gaCurrent.BestFitness.ToString();

                            tempoFitness.Text = bloc.ga.BestFitness.ToString();
                        }, null);

                    });
                }
                else
                {
                    iteration++;

                    DateTime BeforeGeneration = DateTime.Now;
                    textAlgo.GenerationCalcul();
                    DateTime AfterGeneration = DateTime.Now;
                    mysynchronizationContext.Send(o =>
                    {

                        generationTime.Text = (AfterGeneration - BeforeGeneration).Milliseconds.ToString() + "ms";
                        CurrentIteration.Text = iteration.ToString();
                        BestFitness.Text = textAlgo.ga.BestFitness.ToString();
                        // tempoFitness.Text = textAlgo.ga.BestFitness.ToString();
                        bestResultText.Text = new string(textAlgo.bestText);


                    }, null);
                }
            }

            mysynchronizationContext.Send(o =>
            {
                ImageRadio.Enabled = true;
                TextRadio.Enabled = true;
                Play.Text = "Play";
            }, null);

            if (graphicBlocAlgo.GlobalBestFitness(blocs) >= ErrorRate)
            {
                Succed();
            }
        }

        #endregion

        #region graphic


        public List<Color> GetPixels(Image image)
        {
            List<Color> result = new List<Color>();
            Bitmap img = (Bitmap)image;
            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    Color pixel = img.GetPixel(i, j);
                    result.Add(pixel);
                }
            }
            return result;
        }

        private void Succed()
        {
            ExportResult();
        }

        private void PrintDifference()
        {

            Difference.BackgroundImage = graphicBlocAlgo.GenerateBitmapDiffResult(blocs, (Bitmap)targetImage); //PixelDiff((Bitmap)targetImage, BitmapFromDNA(ga.BestGenes));
        }
        private void PrintBestResult()
        {

            BestResult.BackgroundImage = graphicBlocAlgo.GenerateBitmapBestResult(blocs);// BitmapFromDNA(BestGene);
        }

        private void ExportResult()
        {
            Bitmap bitmap = graphicBlocAlgo.GenerateBitmapBestResult(blocs);
            PrintDifference();
            PrintBestResult();
            bitmap.Save(ImageResultPath);
            Process.Start(ImageResultPath);
        }

        private Bitmap BitmapFromDNA(Color[] Colors)
        {
            Bitmap bitmap = new Bitmap(targetImage.Width, targetImage.Height);
            for (int i = 0; i < Colors.Length; i++)
            {
                int x = i / targetImage.Height;
                int y = i - x * targetImage.Height;
                try
                {
                    bitmap.SetPixel(x, y, Colors[i]);
                }
                catch
                { }

            }
            return bitmap;
        }

        unsafe Bitmap PixelDiff(Bitmap a, Bitmap b)
        {
            Bitmap output = new Bitmap(a.Width, a.Height, PixelFormat.Format32bppArgb);
            Rectangle rect = new Rectangle(Point.Empty, a.Size);
            using (var aData = a.LockBitsDisposable(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb))
            using (var bData = b.LockBitsDisposable(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb))
            using (var outputData = output.LockBitsDisposable(rect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb))
            {
                byte* aPtr = (byte*)aData.Scan0;
                byte* bPtr = (byte*)bData.Scan0;
                byte* outputPtr = (byte*)outputData.Scan0;
                int len = aData.Stride * aData.Height;
                for (int i = 0; i < len; i++)
                {
                    // For alpha use the average of both images (otherwise pixels with the same alpha won't be visible)
                    if ((i + 1) % 4 == 0)
                        *outputPtr = (byte)((*aPtr + *bPtr) / 2);
                    else
                        *outputPtr = (byte)~(*aPtr ^ *bPtr);

                    outputPtr++;
                    aPtr++;
                    bPtr++;
                }
            }
            return output;
        }



        #endregion

        #region Utils

        private void AdaptInterfaceToExampleMode()
        {
            ImageRadio.Checked = isImageExample;
            TextRadio.Checked = !isImageExample;

            //image
            Target.Visible = isImageExample;
            BestResult.Visible = isImageExample;
            Difference.Visible = isImageExample;
            bandHeight.Visible = isImageExample;
            label7.Visible = isImageExample;
            label6.Visible = isImageExample;
            tempoFitness.Visible = isImageExample;
            Export.Visible = isImageExample;

            //text
            targetText.Visible = !isImageExample;
            bestResultText.Visible = !isImageExample;
            AllResultText.Visible = !isImageExample;
        }
        public void InitImageGame(Image targetImage, string Text)
        {
            CurrentIteration.Text = "0";
            generationTime.Text = "0";
            BestFitness.Text = "0";
            tempoFitness.Text = "0";
            Image tempo = ResizeImage(targetImage, 100, 100);
            this.targetImage = tempo;
            Target.BackgroundImage = tempo;
            Target.Text = "";
            pixels = GetPixels(tempo);
            uniquePixels = pixels.Distinct().ToList();

            textAlgo = new TextAlgo(Text, (int)populationNum.Value);
            blocs = graphicBlocAlgo.GenerateBlocsFromImage(tempo, (int)populationNum.Value, (int)bandHeight.Value, ErrorRate);
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }



        #endregion


    }

}
