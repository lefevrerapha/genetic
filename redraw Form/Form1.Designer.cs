﻿namespace redraw_Form
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BestResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.populationNum = new System.Windows.Forms.NumericUpDown();
            this.mutationRate = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.CurrentIteration = new System.Windows.Forms.Label();
            this.BestFitness = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Play = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.generationTime = new System.Windows.Forms.Label();
            this.Export = new System.Windows.Forms.Button();
            this.Difference = new System.Windows.Forms.Button();
            this.updateGraphicTimer = new System.Windows.Forms.Timer(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.tempoFitness = new System.Windows.Forms.Label();
            this.bandHeight = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.ImageRadio = new System.Windows.Forms.RadioButton();
            this.TextRadio = new System.Windows.Forms.RadioButton();
            this.targetText = new System.Windows.Forms.RichTextBox();
            this.bestResultText = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.AllResultText = new System.Windows.Forms.ListBox();
            this.Target = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.draw = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.populationNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mutationRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandHeight)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BestResult
            // 
            this.BestResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BestResult.Location = new System.Drawing.Point(114, 216);
            this.BestResult.Name = "BestResult";
            this.BestResult.Size = new System.Drawing.Size(329, 190);
            this.BestResult.TabIndex = 1;
            this.BestResult.Text = "Best Result";
            this.BestResult.UseVisualStyleBackColor = true;
            this.BestResult.Click += new System.EventHandler(this.BestResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(578, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Population";
            // 
            // populationNum
            // 
            this.populationNum.Location = new System.Drawing.Point(653, 27);
            this.populationNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.populationNum.Name = "populationNum";
            this.populationNum.Size = new System.Drawing.Size(120, 20);
            this.populationNum.TabIndex = 4;
            this.populationNum.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // mutationRate
            // 
            this.mutationRate.Location = new System.Drawing.Point(653, 53);
            this.mutationRate.Name = "mutationRate";
            this.mutationRate.Size = new System.Drawing.Size(120, 20);
            this.mutationRate.TabIndex = 6;
            this.mutationRate.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mutationRate.ValueChanged += new System.EventHandler(this.mutationRate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(578, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Mutation Rate";
            // 
            // CurrentIteration
            // 
            this.CurrentIteration.AutoSize = true;
            this.CurrentIteration.Location = new System.Drawing.Point(612, 156);
            this.CurrentIteration.Name = "CurrentIteration";
            this.CurrentIteration.Size = new System.Drawing.Size(25, 13);
            this.CurrentIteration.TabIndex = 7;
            this.CurrentIteration.Text = "100";
            // 
            // BestFitness
            // 
            this.BestFitness.AutoSize = true;
            this.BestFitness.Location = new System.Drawing.Point(779, 156);
            this.BestFitness.Name = "BestFitness";
            this.BestFitness.Size = new System.Drawing.Size(13, 13);
            this.BestFitness.TabIndex = 8;
            this.BestFitness.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(524, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Current Iteration";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(676, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Global Best Fitness";
            // 
            // Play
            // 
            this.Play.Location = new System.Drawing.Point(604, 104);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(75, 23);
            this.Play.TabIndex = 11;
            this.Play.Text = "Play";
            this.Play.UseVisualStyleBackColor = true;
            this.Play.Click += new System.EventHandler(this.Pause_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(524, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "New Iteration time";
            // 
            // generationTime
            // 
            this.generationTime.AccessibleRole = System.Windows.Forms.AccessibleRole.WhiteSpace;
            this.generationTime.AutoSize = true;
            this.generationTime.Location = new System.Drawing.Point(624, 182);
            this.generationTime.Name = "generationTime";
            this.generationTime.Size = new System.Drawing.Size(13, 13);
            this.generationTime.TabIndex = 12;
            this.generationTime.Text = "0";
            // 
            // Export
            // 
            this.Export.Location = new System.Drawing.Point(709, 104);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(75, 23);
            this.Export.TabIndex = 14;
            this.Export.Text = "Export result";
            this.Export.UseVisualStyleBackColor = true;
            this.Export.Click += new System.EventHandler(this.Export_Click);
            // 
            // Difference
            // 
            this.Difference.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Difference.Location = new System.Drawing.Point(494, 213);
            this.Difference.Name = "Difference";
            this.Difference.Size = new System.Drawing.Size(329, 190);
            this.Difference.TabIndex = 15;
            this.Difference.Text = "Difference";
            this.Difference.UseVisualStyleBackColor = true;
            this.Difference.Click += new System.EventHandler(this.Difference_Click);
            // 
            // updateGraphicTimer
            // 
            this.updateGraphicTimer.Tick += new System.EventHandler(this.updateGraphicTimer_Tick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(676, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Tempo Best Fitness";
            // 
            // tempoFitness
            // 
            this.tempoFitness.AutoSize = true;
            this.tempoFitness.Location = new System.Drawing.Point(779, 182);
            this.tempoFitness.Name = "tempoFitness";
            this.tempoFitness.Size = new System.Drawing.Size(13, 13);
            this.tempoFitness.TabIndex = 16;
            this.tempoFitness.Text = "0";
            // 
            // bandHeight
            // 
            this.bandHeight.Location = new System.Drawing.Point(653, 79);
            this.bandHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bandHeight.Name = "bandHeight";
            this.bandHeight.Size = new System.Drawing.Size(120, 20);
            this.bandHeight.TabIndex = 19;
            this.bandHeight.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(578, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Band hieght";
            // 
            // ImageRadio
            // 
            this.ImageRadio.AutoSize = true;
            this.ImageRadio.Checked = true;
            this.ImageRadio.Location = new System.Drawing.Point(457, 25);
            this.ImageRadio.Name = "ImageRadio";
            this.ImageRadio.Size = new System.Drawing.Size(96, 17);
            this.ImageRadio.TabIndex = 21;
            this.ImageRadio.TabStop = true;
            this.ImageRadio.Text = "Image example";
            this.ImageRadio.UseVisualStyleBackColor = true;
            this.ImageRadio.Click += new System.EventHandler(this.ImageRadio_CheckedChanged);
            // 
            // TextRadio
            // 
            this.TextRadio.AutoSize = true;
            this.TextRadio.Location = new System.Drawing.Point(457, 48);
            this.TextRadio.Name = "TextRadio";
            this.TextRadio.Size = new System.Drawing.Size(88, 17);
            this.TextRadio.TabIndex = 22;
            this.TextRadio.Text = "Text example";
            this.TextRadio.UseVisualStyleBackColor = true;
            this.TextRadio.Click += new System.EventHandler(this.ImageRadio_CheckedChanged);
            // 
            // targetText
            // 
            this.targetText.Location = new System.Drawing.Point(114, 20);
            this.targetText.Name = "targetText";
            this.targetText.Size = new System.Drawing.Size(329, 112);
            this.targetText.TabIndex = 24;
            this.targetText.Text = "Ashem Vohu. I profess myself a Mazda-worshipper and a Zoroastrian, opposing the D" +
    "aevas, accepting the Ahuric doctrine. For Hawan";
            this.targetText.TextChanged += new System.EventHandler(this.targetText_TextChanged);
            // 
            // bestResultText
            // 
            this.bestResultText.Enabled = false;
            this.bestResultText.Location = new System.Drawing.Point(114, 217);
            this.bestResultText.Name = "bestResultText";
            this.bestResultText.Size = new System.Drawing.Size(329, 83);
            this.bestResultText.TabIndex = 25;
            this.bestResultText.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Target";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 220);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Best  Result";
            // 
            // AllResultText
            // 
            this.AllResultText.FormattingEnabled = true;
            this.AllResultText.Location = new System.Drawing.Point(494, 213);
            this.AllResultText.Name = "AllResultText";
            this.AllResultText.Size = new System.Drawing.Size(329, 199);
            this.AllResultText.TabIndex = 28;
            // 
            // Target
            // 
            this.Target.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Target.Location = new System.Drawing.Point(114, 20);
            this.Target.Name = "Target";
            this.Target.Size = new System.Drawing.Size(329, 190);
            this.Target.TabIndex = 0;
            this.Target.Text = "Base image";
            this.Target.UseVisualStyleBackColor = true;
            this.Target.Click += new System.EventHandler(this.Target_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(867, 475);
            this.tabControl1.TabIndex = 29;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.BestResult);
            this.tabPage1.Controls.Add(this.AllResultText);
            this.tabPage1.Controls.Add(this.Target);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.populationNum);
            this.tabPage1.Controls.Add(this.bestResultText);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.targetText);
            this.tabPage1.Controls.Add(this.mutationRate);
            this.tabPage1.Controls.Add(this.TextRadio);
            this.tabPage1.Controls.Add(this.CurrentIteration);
            this.tabPage1.Controls.Add(this.ImageRadio);
            this.tabPage1.Controls.Add(this.BestFitness);
            this.tabPage1.Controls.Add(this.bandHeight);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.Play);
            this.tabPage1.Controls.Add(this.tempoFitness);
            this.tabPage1.Controls.Add(this.generationTime);
            this.tabPage1.Controls.Add(this.Difference);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.Export);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(859, 449);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.draw);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(859, 449);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // draw
            // 
            this.draw.Location = new System.Drawing.Point(22, 19);
            this.draw.Name = "draw";
            this.draw.Size = new System.Drawing.Size(75, 23);
            this.draw.TabIndex = 0;
            this.draw.Text = "draw";
            this.draw.UseVisualStyleBackColor = true;
            this.draw.Click += new System.EventHandler(this.draw_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 511);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.populationNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mutationRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandHeight)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BestResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown populationNum;
        private System.Windows.Forms.NumericUpDown mutationRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label CurrentIteration;
        private System.Windows.Forms.Label BestFitness;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Play;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label generationTime;
        private System.Windows.Forms.Button Export;
        private System.Windows.Forms.Button Difference;
        private System.Windows.Forms.Timer updateGraphicTimer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label tempoFitness;
        private System.Windows.Forms.NumericUpDown bandHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton ImageRadio;
        private System.Windows.Forms.RadioButton TextRadio;
        private System.Windows.Forms.RichTextBox targetText;
        private System.Windows.Forms.RichTextBox bestResultText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox AllResultText;
        private System.Windows.Forms.Button Target;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button draw;
        private System.Windows.Forms.Button button1;
    }
}

