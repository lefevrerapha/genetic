﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace redraw_Form
{
    public class graphicBlocAlgo
    {
        #region properties
        private int Width;//largeru
        private int Height;//longueur
        private Image targetImage;
        private List<Color> targetPixels;
        private List<Color> uniqueTargetPixel;
        public GeneticAlgorithm<Color> ga;
        private Random random;
        private int population;
        private float elitismRate;
        private float mutationRate;
        private float newElementRate;
        private Color[] BestGene = new Color[] { };


        private double ErrorRate;//0.9999f;
        private static int splitHeight;
        #endregion

        #region constructor
        public graphicBlocAlgo(Image targetImage, int population, double ErrorRate = 0.999f, float elitismRate = 0.01f, float mutationRate = 0.01f, float newElementRate = 0.01f)
        {
            this.ErrorRate = ErrorRate;
            this.targetImage = targetImage;
            Width = targetImage.Width;
            Height = targetImage.Height;
            this.targetPixels = GetPixels(targetImage);
            uniqueTargetPixel = targetPixels.Distinct().ToList();
            this.population = population;
            this.elitismRate = elitismRate;
            this.mutationRate = mutationRate;
            this.newElementRate = newElementRate;
            random = new Random();
            ga = new GeneticAlgorithm<Color>(population, targetPixels.Count, random, GetRandomPixel, FitnessFunction, elitismRate, mutationRate, newElementRate);

        }

        #endregion

        #region olgo
        private Color GetRandomPixel()
        {
            return targetPixels[random.Next(targetPixels.Count)];
        }

        private double FitnessFunction(DNA<Color> dna)
        {
            double exactScore = 0;
            double partialScore = 0;
            double distanceScore = 0;
            List<Color> temppixels = new List<Color>();
            temppixels.AddRange(targetPixels);

            for (int i = 0; i < dna.Genes.Length; i++)
            {

                //distance

                double distance = DistanceBetweenColor(dna.Genes[i], targetPixels[i]);

                if (distance == 1)
                {
                    exactScore += distance;
                    if (temppixels.LastIndexOf(dna.Genes[i]) >= 0)
                        temppixels.RemoveAt(temppixels.LastIndexOf(dna.Genes[i]));
                }
                else
                {
                    distanceScore += distance;
                    if (temppixels.Contains(dna.Genes[i]))
                    {
                        // partialScore += 1 * (1 - distance);
                        if (temppixels.LastIndexOf(dna.Genes[i]) >= 0)
                            temppixels.RemoveAt(temppixels.LastIndexOf(dna.Genes[i]));
                    }
                }
            }
            exactScore += distanceScore;
            exactScore /= targetPixels.Count;
            partialScore /= targetPixels.Count;

            partialScore = partialScore / (targetPixels.Count - temppixels.Count + 1);
            double score = exactScore + partialScore;
            return score;
        }

        public double GenerationCalcul()
        {
            ga.NewGeneration();
            if (BestGene != ga.BestGenes)
                BestGene = ga.BestGenes;
            return ga.BestFitness;

        }

        #endregion

        #region utils

        unsafe static Bitmap PixelDiff(Bitmap a, Bitmap b)
        {
            Bitmap output = new Bitmap(a.Width, a.Height, PixelFormat.Format32bppArgb);
            try
            {
                Rectangle rect = new Rectangle(Point.Empty, a.Size);
                using (var aData = a.LockBitsDisposable(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb))
                using (var bData = b.LockBitsDisposable(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb))
                using (var outputData = output.LockBitsDisposable(rect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb))
                {
                    byte* aPtr = (byte*)aData.Scan0;
                    byte* bPtr = (byte*)bData.Scan0;
                    byte* outputPtr = (byte*)outputData.Scan0;
                    int len = aData.Stride * aData.Height;
                    for (int i = 0; i < len; i++)
                    {
                        // For alpha use the average of both images (otherwise pixels with the same alpha won't be visible)
                        if ((i + 1) % 4 == 0)
                            *outputPtr = (byte)((*aPtr + *bPtr) / 2);
                        else
                            *outputPtr = (byte)~(*aPtr ^ *bPtr);

                        outputPtr++;
                        aPtr++;
                        bPtr++;
                    }
                }
            }
            catch { }
            return output;
        }

        private Bitmap BitmapFromDNA()
        {
            Color[] Colors = BestGene;
            Bitmap bitmap = new Bitmap(targetImage.Width, targetImage.Height);
            for (int i = 0; i < Colors.Length; i++)
            {
                int x = i / targetImage.Height;
                int y = i - x * targetImage.Height;
                try
                {
                    bitmap.SetPixel(x, y, Colors[i]);
                }
                catch
                { }

            }
            return bitmap;
        }

        public List<Color> GetPixels(Image image)
        {
            List<Color> result = new List<Color>();
            Bitmap img = (Bitmap)image;
            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    Color pixel = img.GetPixel(i, j);
                    result.Add(pixel);
                }
            }
            return result;
        }
        /// <summary>
        /// distance between 0/1, 1is the same 0 is completely different
        /// </summary>
        /// <param name="color1"></param>
        /// <param name="color2"></param>
        /// <returns></returns>
        public double DistanceBetweenColor(Color color1, Color color2)
        {
            int redDifference;
            int greenDifference;
            int blueDifference;

            redDifference = color1.R - color2.R;
            greenDifference = color1.G - color2.G;
            blueDifference = color1.B - color2.B;

            double partialscore = redDifference * redDifference + greenDifference * greenDifference + blueDifference * blueDifference;
            partialscore = Math.Sqrt(partialscore);

            partialscore = (double)(765 - partialscore) / 765;

            return partialscore;
        }
        #endregion

        #region static method
        internal static double GlobalBestFitness(List<graphicBlocAlgo> blocs)
        {
            double bestGlobalFitness = 0;
            foreach (graphicBlocAlgo bloc in blocs)
            {
                bestGlobalFitness += bloc.ga.BestFitness;
            }
            bestGlobalFitness /= blocs.Count;
            return bestGlobalFitness;
        }

        public static Bitmap GenerateBitmapDiffResult(List<graphicBlocAlgo> Blocs, Bitmap originalFullBitmap)
        {
            Bitmap bitmap;
            Bitmap bestResultBitmap = GenerateBitmapBestResult(Blocs);
            bitmap = PixelDiff(originalFullBitmap, bestResultBitmap);

            return bitmap;

        }

        public static Bitmap GenerateBitmapBestResult(List<graphicBlocAlgo> Blocs)
        {
            List<Bitmap> bitmaps = Blocs.Select(x => x.BitmapFromDNA()).ToList();
            Bitmap bitmap = RegroupImages(bitmaps, graphicBlocAlgo.splitHeight);


            //foreach (Bloc bloc in Blocs)
            //{
            //    bitmap = bloc.BitmapFromDNA();//todo improve
            //}
            return bitmap;
        }

        public static List<graphicBlocAlgo> GenerateBlocsFromImage(Image targetImage, int population, int splitHeight, double ErrorRate = 0.999f, float elitismRate = 0.01f, float mutationRate = 0.01f, float newElementRate = 0.01f)
        {
            List<graphicBlocAlgo> blocs = new List<graphicBlocAlgo>();
            graphicBlocAlgo.splitHeight = splitHeight;
            List<Bitmap> imageSplitted = SplitImage((Bitmap)targetImage, graphicBlocAlgo.splitHeight);
            foreach (Bitmap bitmap in imageSplitted)
            {
                blocs.Add(new graphicBlocAlgo((Image)bitmap, population, ErrorRate, elitismRate, mutationRate, newElementRate));
            }
            //blocs.Add(new Bloc(targetImage, population, ErrorRate, elitismRate, mutationRate, newElementRate));//todo improve
            //blocs.Add(new Bloc(targetImage, population, ErrorRate, elitismRate, mutationRate, newElementRate));//todo improve
            return blocs;

        }


        public static List<Bitmap> SplitImage(Bitmap sourceBitmap, int splitHeight)
        {
            Size dimension = sourceBitmap.Size;
            Rectangle sourceRectangle = new Rectangle(0, 0, dimension.Width, splitHeight);
            Rectangle targetRectangle = new Rectangle(0, 0, dimension.Width, splitHeight);

            List<Bitmap> results = new List<Bitmap>();

            while (sourceRectangle.Top < dimension.Height)
            {
                Bitmap pageBitmap = new Bitmap(targetRectangle.Size.Width, sourceRectangle.Bottom < dimension.Height ?
                    targetRectangle.Size.Height
                    :
                    dimension.Height - sourceRectangle.Top, PixelFormat.Format32bppArgb);

                using (Graphics g = Graphics.FromImage(pageBitmap))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                    g.DrawImage(sourceBitmap, targetRectangle, sourceRectangle, GraphicsUnit.Pixel);
                }
                sourceRectangle.Y += sourceRectangle.Height;
                results.Add(pageBitmap);
            }

            return results;
        }

        public static Bitmap RegroupImages(List<Bitmap> bitmaps, int splitHeight)
        {
            int i = 0;
            Bitmap newBigBitmap = new Bitmap(bitmaps.First().Width, bitmaps.Count * splitHeight);
            foreach (Bitmap bitmap in bitmaps)
            {
                using (Graphics g = Graphics.FromImage(newBigBitmap))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                    g.DrawImage(bitmap, 0, i * splitHeight);
                }
                i++;
            }
            return newBigBitmap;
        }
        #endregion


    }
    #region class
    static class Extensions
    {
        public static DisposableImageData LockBitsDisposable(this Bitmap bitmap, Rectangle rect, ImageLockMode flags, PixelFormat format)
        {
            return new DisposableImageData(bitmap, rect, flags, format);
        }

        public class DisposableImageData : IDisposable
        {
            private readonly Bitmap _bitmap;
            private readonly BitmapData _data;

            internal DisposableImageData(Bitmap bitmap, Rectangle rect, ImageLockMode flags, PixelFormat format)
            {

                _bitmap = bitmap;
                _data = bitmap.LockBits(rect, flags, format);
            }

            public void Dispose()
            {
                _bitmap.UnlockBits(_data);
            }

            public IntPtr Scan0
            {
                get { return _data.Scan0; }
            }

            public int Stride
            {
                get { return _data.Stride; }
            }

            public int Width
            {
                get { return _data.Width; }
            }

            public int Height
            {
                get { return _data.Height; }
            }

            public PixelFormat PixelFormat
            {
                get { return _data.PixelFormat; }
            }

            public int Reserved
            {
                get { return _data.Reserved; }
            }
        }
    }
    #endregion
}
