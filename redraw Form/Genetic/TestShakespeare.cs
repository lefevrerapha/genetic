using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

public class TestShakespeare
{
   

    #region new painting game
    private string targetString;//= "To be, or not to be, that is the question.";
    //private string validCharacters;//= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.|!#$%&/()=? ";
    private int populationSize;//= 200;
    private float mutationRate;// = 0.01f;
    private float newElementRate;// = 0.01f;
    private float elitismRate;// = 0.01f;


    private Label success;
    private int numCharsPerText = 15000;
    private Label targetText;
    private Label bestText;
    private Label bestFitnessText;
    private Label numGenerationsText;
    //private Transform populationTextParent;
    private Label textPrefab;




    private string validCharacters;
    private GeneticAlgorithm<char> ga;
    private System.Random random;

    void Start()
    {
        //MyImage.AddComponent(typeof(Image));
        //img1 = Resources.Load<Sprite>("ARInfo/Magazine1/1");
        //MyImage.GetComponent<Image>().sprite = img1;
        //Debug.Log("Test script started");

        success.Text = "";
        targetText.Text = targetString;
        //if (string.IsNullOrEmpty(targetString))
        //{
        //    Debug.LogError("Target string is null or empty");
        //    this.enabled = false;
        //}
        //Debug.LogWarning("populationSize: " + populationSize);
        //Debug.LogWarning("mutationRate: " + mutationRate);
        //Debug.LogWarning("elitism: " + elitismRate);

        random = new System.Random();
     //   ga = new GeneticAlgorithm<char>(populationSize, targetString.Length, random, GetRandomCharacter, FitnessFunction, elitismRate, mutationRate, newElementRate);

    }

    void Update()
    {
        ga.NewGeneration();
        UpdateText(ga.BestGenes, ga.BestFitness, ga.Generation, ga.Population.Count, (j) => ga.Population[j].Genes);

        if (ga.BestFitness == 1)
        {
            bestFitnessText.Text = "";
            numGenerationsText.Text = "";
            success.Text = "You win in " + ga.Generation + " step!!";
            bestText.Text = "";
           // this.enabled = false;

        }
    }

    private char GetRandomCharacter()
    {
        int i = random.Next(validCharacters.Length);
        return validCharacters[i];
    }


    private float FitnessFunction(DNA<char> dna)
    {
        float score = 0;
        //float dualScore = 0;
        float exactScore = 0;
        float partialScore = 0;
        string tempTarget = targetString;
        // DNA<char> dna = ga.Population[index];

        //int number = string.CompareOrdinal(tempo, "");
        //int MaxNumber =  string.CompareOrdinal(targetString, "");

        for (int i = 0; i < dna.Genes.Length; i++)
        {
            //if (i < dna.Genes.Length - 1)
            //{
            //    string tempo = (new string(dna.Genes)).Substring(i, 2);
            //    if (targetString.Contains(tempo))
            //    {
            //        dualScore += 1;                
            //    }
            //}
            if (dna.Genes[i] == targetString[i])
            {
                exactScore += 1;
                if (tempTarget.LastIndexOf(dna.Genes[i].ToString()) >= 0)
                    tempTarget = tempTarget.Remove(tempTarget.LastIndexOf(dna.Genes[i].ToString()), 1);

            }
            else if (tempTarget.Contains(dna.Genes[i].ToString()))
            {
                partialScore += 1;
                tempTarget = tempTarget.Remove(tempTarget.LastIndexOf(dna.Genes[i].ToString()), 1);
            }
        }
        //score
        exactScore /= targetString.Length;
        partialScore /= targetString.Length;

        //  score = exactScore + partialScore / 2;
        // score /= targetString.Length;

        //make it exponential
        //exactScore = (Mathf.Pow(2, exactScore) - 1) / (2 - 1);

        //float coef = 1 + (float)(Math.Cos(3 * exactScore) + 1);
        partialScore = partialScore / (targetString.Length - tempTarget.Length + 1);

        score = exactScore + partialScore;
        score =(float) (Math.Pow(2, score) - 1) / (2 - 1);
        return score;
    }

    private void InitializeValidCharacter()
    {
        int maxAsciiCarIndex = 200;
        validCharacters = string.Empty;
        {
            for (int j = char.MinValue; j <= char.MaxValue; j++)
            {
                if (j > maxAsciiCarIndex)
                    return;
                char c = Convert.ToChar(j);
                if (!char.IsControl(c))
                {
                    validCharacters = string.Concat(validCharacters, c.ToString());
                    // validCharacters.Insert(validCharacters.Length + 1, c.ToString());
                }
            }
        }
    }
    private int numCharsPerTextObj;
    private List<Label> textList = new List<Label>();

    void Awake()
    {

        //DebugGUI.SetGraphProperties("evolutionGraph", "Evolution of the algorithm", 0, 1, 9, new Color(0, 1, 1), true);
        InitializeValidCharacter();
        //validCharacters= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.|!#$%&/()=? ";
        numCharsPerTextObj = numCharsPerText / validCharacters.Length;
        if (numCharsPerTextObj > populationSize)
            numCharsPerTextObj = populationSize;

        //int numTextObjects = Mathf.CeilToInt((float)populationSize / numCharsPerTextObj);

        //for (int i = 0; i < numTextObjects; i++)
        //{
        //    textList.Add(Instantiate(textPrefab, populationTextParent));
        //}
    }

    void OnDestroy()
    {
        // DebugGUI.RemoveGraph("evolutionGraph");
    }
    private void UpdateText(char[] bestGenes, double bestFitness, int generation, int populationSize, Func<int, char[]> getGenes)
    {
        bestText.Text = CharArrayToString(bestGenes);
        bestFitnessText.Text = bestFitness.ToString();

        numGenerationsText.Text = generation.ToString();

        for (int i = 0; i < textList.Count; i++)
        {
            var sb = new StringBuilder();
            int endIndex = i == textList.Count - 1 ? populationSize : (i + 1) * numCharsPerTextObj;
            for (int j = i * numCharsPerTextObj; j < endIndex; j++)
            {
                foreach (var c in getGenes(j))
                {
                    sb.Append(c);
                }
                if (j < endIndex - 1) sb.AppendLine();
            }

            textList[i].Text = sb.ToString();
        }
    }

    private string CharArrayToString(char[] charArray)
    {
        var sb = new StringBuilder();
        foreach (var c in charArray)
        {
            sb.Append(c);
        }

        return sb.ToString();
    }
    #endregion
}
