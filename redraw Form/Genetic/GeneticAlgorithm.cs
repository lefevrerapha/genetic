using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

public class GeneticAlgorithm<T>
{
    public List<DNA<T>> Population { get; private set; }
    public int Generation { get; private set; }
    public double BestFitness { get; private set; }
    public T[] BestGenes { get; private set; }

    public float ElitismRate;
    public float MutationRate;
    public float NewElementRate;

    private List<DNA<T>> newPopulation;
    private Random random;
    private double fitnessSum;
    private int dnaSize;
    private Func<T> getRandomGene;
    private Func<DNA<T>, double> fitnessFunction;
    /// <summary>
    /// Alorithm that will find the solution 
    /// </summary>
    /// <param name="populationSize"></param>
    /// <param name="dnaSize"></param>
    /// <param name="random"></param>
    /// <param name="getRandomGene"></param>
    /// <param name="fitnessFunction"></param>
    /// <param name="elitismRate"></param>
    /// <param name="mutationRate"></param>
    /// <param name="newElementRate">% of the population that will be new at each update</param>
    public GeneticAlgorithm(int populationSize, int dnaSize, Random random, Func<T> getRandomGene, Func<DNA<T>, double> fitnessFunction,
        float elitismRate = 0.01f, float mutationRate = 0.01f, float newElementRate = 0.01f)
    {
        Generation = 1;
        ElitismRate = elitismRate;
        MutationRate = mutationRate;
        NewElementRate = newElementRate;
        Population = new List<DNA<T>>(populationSize);
        newPopulation = new List<DNA<T>>(populationSize);
        this.random = random;
        this.dnaSize = dnaSize;
        this.getRandomGene = getRandomGene;
        this.fitnessFunction = fitnessFunction;

        BestGenes = new T[dnaSize];

        for (int i = 0; i < populationSize; i++)
        {
            Population.Add(new DNA<T>(dnaSize, random, getRandomGene, fitnessFunction, shouldInitGenes: true));
        }
    }

    public void NewGeneration(bool crossoverNewDNA = true)
    {
        if (NewElementRate >= 1)
            Console.WriteLine("Error, NewElementRate can't be > " + 1);

        int numPopulationToMix = Population.Count - (int)(NewElementRate * Population.Count);

        if (Population.Count > 0)
        {
            CalculateFitness();
            Population.Sort(CompareDNA);
        }
        newPopulation.Clear();



        for (int i = 0; i < Population.Count; i++)
        {
            //we keep best solution of previosu round
            if (i < 1 && i < Population.Count)
            {
                newPopulation.Add(Population[i]);
            }
            //we mixed previous DNA to the new generation
            else if (i < numPopulationToMix && crossoverNewDNA)
            {
                DNA<T> parent1 = ChooseParent();
                DNA<T> parent2 = ChooseParent();

                //can crash if parent1 is null
                DNA<T> child = parent1.Crossover(parent2);

                child.Mutate(MutationRate);

                newPopulation.Add(child);
            }
            // We add new DNA in the new generation
            else
            {
                newPopulation.Add(new DNA<T>(dnaSize, random, getRandomGene, fitnessFunction, shouldInitGenes: true));
            }
        }

        List<DNA<T>> tmpList = Population;
        Population = newPopulation;
        newPopulation = tmpList;

        Generation++;
    }

    private int CompareDNA(DNA<T> a, DNA<T> b)
    {
        if (a.Fitness > b.Fitness)
        {
            return -1;
        }
        else if (a.Fitness < b.Fitness)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    private void CalculateFitness()
    {
        fitnessSum = 0;
        DNA<T> best = Population[0];

        ParallelOptions parallelOptions = new ParallelOptions();
        parallelOptions.MaxDegreeOfParallelism = 8;
        Parallel.ForEach(Population, parallelOptions, Dna =>
        {
            fitnessSum += Dna.CalculateFitness(Dna);
            if (Dna.Fitness > best.Fitness)
            {
                best = Dna;
            }
        });

        //for (int i = 0; i < Population.Count; i++)
        //{
        //    fitnessSum += Population[i].CalculateFitness(Population[i]);

        //    if (Population[i].Fitness > best.Fitness)
        //    {
        //        best = Population[i];
        //    }
        //}

        BestFitness = best.Fitness;
        best.Genes.CopyTo(BestGenes, 0);
    }

    /// <summary>
    /// We choose random parent in previous generation based on their fitness, we randomly try to get good parents
    /// It will not choose parent with Fitness = 0, or at if nothing else is possible
    /// </summary>
    /// <returns></returns>
    private DNA<T> ChooseParent()
    {


        double randomNumber = random.NextDouble() * fitnessSum;
        int elitePopulation = (int)(ElitismRate * Population.Count);
        for (int i = 0; i < elitePopulation; i++)
        {
            if (randomNumber < Population[i].Fitness)
            {
                return Population[i];
            }

            randomNumber -= Population[i].Fitness;
        }

        //if failed to get a good parent, we take the first one
        return Population[0];
    }

}
