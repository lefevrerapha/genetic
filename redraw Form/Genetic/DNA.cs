using System;

public class DNA<T>
{
    public T[] Genes { get; private set; }
    public double Fitness { get; private set; }

    private Random random;
    private Func<T> getRandomGene;
    private Func<DNA<T>, double> fitnessFunction;

    public DNA(int size, Random random, Func<T> getRandomGene, Func<DNA<T>, double> fitnessFunction, bool shouldInitGenes = true)
    {
        Genes = new T[size];
        this.random = random;
        this.getRandomGene = getRandomGene;
        this.fitnessFunction = fitnessFunction;

        if (shouldInitGenes)
        {
            for (int i = 0; i < Genes.Length; i++)
            {
                Genes[i] = getRandomGene();
            }
        }
    }

    public double CalculateFitness(DNA<T> dNA)
    {
        Fitness = fitnessFunction(dNA);
        return Fitness;
    }

    /// <summary>
    /// We mixe randomly genes of both parent to get a new child
    /// </summary>
    /// <param name="otherParent"></param>
    /// <returns></returns>
    public DNA<T> Crossover(DNA<T> otherParent)
    {
        DNA<T> child = new DNA<T>(Genes.Length, random, getRandomGene, fitnessFunction, shouldInitGenes: false);

        for (int i = 0; i < Genes.Length; i++)
        {
            child.Genes[i] = random.NextDouble() < 0.5 ? Genes[i] : otherParent.Genes[i];
        }

        //child.CalculateFitness(child);
        return child;
    }
    /// <summary>
    /// We modified the gene of the DNA randomly compare to Mutation Rate
    /// </summary>
    /// <param name="mutationRate"></param>
    public void Mutate(float mutationRate)
    {
        //int totalMutation = 0;
        //int internalMutationAppear = 0;
        T[] previousGen = this.Genes;
        for (int i = 0; i < Genes.Length; i++)
        {
            double rand = random.NextDouble();
            if (rand < mutationRate)
            {
                //totalMutation++;
                //si fitness = 0=> need random gen
                //if fitness~1 need previous gen in new order
                if (rand < mutationRate * 0.5)
                {
                    //  internalMutationAppear++;
                    Genes[i] = previousGen[random.Next(Genes.Length)];
                }
                else
                    Genes[i] = getRandomGene();
            }
        }
        //float internalMutationRate = (float)internalMutationAppear / totalMutation * 100;
    }
}