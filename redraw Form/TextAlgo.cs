﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace redraw_Form
{
    public class TextAlgo
    {

        #region properties
        private string targetText;
        List<Char> validCharacters;
        public GeneticAlgorithm<char> ga;
        private Random random;
        private int population;
        private float elitismRate;
        private float mutationRate;
        private float newElementRate;
        private double ErrorRate;
        public char[] bestText;
        #endregion


        #region constructor
        public TextAlgo(string targetText, int population, double ErrorRate = 1, float elitismRate = 0.01f, float mutationRate = 0.01f, float newElementRate = 0.01f)
        {
            this.ErrorRate = ErrorRate;
            this.targetText = targetText;
            this.population = population;
            this.elitismRate = elitismRate;
            this.mutationRate = mutationRate;
            this.newElementRate = newElementRate;
            random = new Random();
            validCharacters = GenerateListOfPossibleChar();
            ga = new GeneticAlgorithm<char>(population, targetText.Count(), random, GetRandomCharacter, FitnessFunction, elitismRate, mutationRate, newElementRate);

        }
        #endregion

        #region algo
        private char GetRandomCharacter()
        {
            int i = random.Next(validCharacters.Count);
            return validCharacters[i];
        }

        public List<Char> GenerateListOfPossibleChar()
        {
            List<Char> printableChars = new List<char>();
            for (int i = char.MinValue; i <= char.MaxValue; i++)
            {
                if (i > 382)
                    continue;
                char c = Convert.ToChar(i);
                if (!char.IsControl(c))
                {
                    printableChars.Add(c);
                }
            }
            return printableChars;
        }

        private double FitnessFunction(DNA<char> dna)
        {
            double score = 0;

            for (int i = 0; i < dna.Genes.Length; i++)
            {
                if (dna.Genes[i] == targetText[i])
                {
                    score += 1;
                }
            }

            score /= targetText.Length;

            score = (Math.Pow(2, score) - 1) / (2 - 1);

            return score;
        }

        public double GenerationCalcul()
        {
            ga.NewGeneration();
            if (bestText != ga.BestGenes)
                bestText = ga.BestGenes;
            return ga.BestFitness;

        }
        #endregion
    }
}
