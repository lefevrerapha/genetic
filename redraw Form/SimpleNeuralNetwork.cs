﻿using NeuralNetworkCSharp;
using NeuralNetworkCSharp.ActivationFunctions;
using NeuralNetworkCSharp.InputFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace redraw_Form
{

    public class myNeuralNetwork
    {
        public enum Deepness
        {
            Simple = 0,
            Normal = 1,
            Deep = 2,
            ReallyDeep = 3
        }
        public double[][] inputs;
        public double[][] expectedOutputs;
        private SimpleNeuralNetwork simpleNeuralNetwork;
        public myNeuralNetwork(int numberOfInputNeurons, int numberOfOutputNeurons, Deepness deepness)
        {
            simpleNeuralNetwork = new SimpleNeuralNetwork(numberOfInputNeurons);
            var layerFactory = new NeuralLayerFactory();
            for (int i = 0; i < (int)deepness; i++)
            {
                simpleNeuralNetwork.AddLayer(layerFactory.CreateNeuralLayer(2 * (i + 1), new RectifiedActivationFuncion(), new WeightedSumFunction()));
            }

            simpleNeuralNetwork.AddLayer(layerFactory.CreateNeuralLayer(numberOfOutputNeurons, new SigmoidActivationFunction(0.99), new WeightedSumFunction()));
        }

        public void Train(double[][] inputs, double[][] expectedOutputs, int numberOfEpochs)
        {
            this.inputs = inputs;
            this.expectedOutputs = expectedOutputs;
            //expectedOutputs= new double[][] {
            //        new double[] { 0 },
            //        new double[] { 1 },
            //        new double[] { 1 },
            //        new double[] { 0 },
            //        new double[] { 1 },
            //        new double[] { 0 },
            //        new double[] { 0 },
            //});
            simpleNeuralNetwork.PushExpectedValues(expectedOutputs);

            //inputs = new double[][] {
            //        new double[] { 150, 2, 0 },
            //        new double[] { 1002, 56, 1 },
            //        new double[] { 1060, 59, 1 },
            //        new double[] { 200, 3, 0 },
            //        new double[] { 300, 3, 1 },
            //        new double[] { 120, 1, 0 },
            //        new double[] { 80, 1, 0 });
            //numberOfEpochs= 10000);

            simpleNeuralNetwork.Train(inputs, numberOfEpochs);
        }

        public List<double> Use(double[] inputs)
        {
            simpleNeuralNetwork.PushInputValues(inputs);
            var outputs = simpleNeuralNetwork.GetOutput();
            return outputs;
        }
    }
}
